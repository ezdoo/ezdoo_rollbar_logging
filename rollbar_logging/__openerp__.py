# -*- coding: utf-8 -*-
{
    'name': "rollbar_logging",

    'summary': """
        Add handler to logging with rollbar""",

    'description': """
        Integrate Rollbar with python logging.
        Oddo log and tracing send to rollbar service.
    """,

    'author': "BarraDev Consulting",
    'website': "http://www.barradev.com",

    'category': 'Hidden',
    'version': '0.1',
    'external_dependencies': {
        'python': ['rollbar.logger'],
    },

}
