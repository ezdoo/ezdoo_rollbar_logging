# -*- coding: utf-8 -*-
"""
Rollbar logging handler for Odoo.

"""

import logging
import os
import threading

import rollbar
from rollbar.logger import RollbarHandler
from openerp.tools import config
from openerp.http import request as odoo_request
from openerp import release


def _data_hook(request, data):
    """
    Add Odoo information to rollbar data.
    """

    data['framework'] = "%s" % release.product_name
    data['framework.version'] = release.version
    data['framework.db'] = threading.current_thread().dbname
    if odoo_request and odoo_request.env.user:
        user = odoo_request.env.user
        person = {'id': user.id, 'username': user.login}
        if user.email:
            person['email'] = user.email
        data['person'] = person


def _odoo_request():
    """
    Return Odoo http request object to rollbar.
    """
    try:
        http_request = odoo_request.httprequest
    except RuntimeError as err:
        if 'object unbound' in err:
            http_request = None
        else:
            raise err
    
    return http_request


def logging_with_rollbar():
    """
    Handling Odoo logging with Rollbar
    """

    rollbar_config = {'allow_logging_basic_config': False}
    rollbar.init(ACCESS_TOKEN, ENVIRONMENT, **rollbar_config)

    rollbar_handler = RollbarHandler(history_size=5)
    rollbar_handler.setLevel(logging.WARNING)

    # rollbar_handler.setHistoryLevel(logging.DEBUG)

    logging.getLogger(ROOT_LOGGER).addHandler(rollbar_handler)
    logging.getLogger(__name__).info('(%s) %s', ROOT_LOGGER, ACCESS_TOKEN)


try:
    ACCESS_TOKEN = config['rollbar_token']
except KeyError:
    try:
        ACCESS_TOKEN = os.environ['ROLLBAR_TOKEN']
    except KeyError:
        ACCESS_TOKEN = None

try:
    ENVIRONMENT = config['rollbar_environment']
except KeyError:
    try:
        ENVIRONMENT = os.environ['ROLLBAR_ENVIRONMENT']
    except KeyError:
        ENVIRONMENT = 'development'

try:
    ROOT_LOGGER = config['rollbar_root_logger']
except KeyError:
    try:
        ROOT_LOGGER = os.environ['ROLLBAR_ROOT_LOGGER']
    except KeyError:
        ROOT_LOGGER = ''

if ACCESS_TOKEN:
    rollbar.BASE_DATA_HOOK = _data_hook
    rollbar.get_request = _odoo_request
    logging_with_rollbar()
